from fastapi.testclient import TestClient
from main import app
from routers.trips import TripQueries
from authenticator import authenticator


client = TestClient(app)


def fake_get_current_account_data():
    return {
        "id": "fakeuser"
    }


class FakeTripQueries:
    def get_stop(self, trip_id: str, stop_id: str, user_id: str):
        return {
            "name": "Venice",
            "street": "1",
            "state": "2",
            "city": "Venice",
            "description": "italy",
            "id": "64493a5680c5629beb5d04ab",
            "picture_url": "fakepic"
        }

    def get_trip(self, trip_id: str, user_id: str):
        return {
            "name": "EuroTrip",
            "picture_url": "fakepic",
            "start_date": "1",
            "end_date": "2",
            "description": "A backpacking trip all over Europe",
            "id": "644938c180c5629beb5d04a8",
            "stops": [{
                "name": "Venice",
                "street": "1",
                "state": "2",
                "city": "Venice",
                "description": "italy",
                "id": "64493a5680c5629beb5d04ab",
                "picture_url": "fakepic"
                },
                {
                "name": "Italy",
                "street": "1",
                "state": "2",
                "city": "Trieste",
                "description": "A backpacking trip all over Europe",
                "id": "64493ab280c5629beb5d04ac",
                "picture_url": "fakepic"
                }
            ]
        }

    def get_all_trips(self, user_id: str):
        return {
            "trips": [{
                "name": "EuroTrip",
                "picture_url": "fakepic",
                "start_date": "1",
                "end_date": "2",
                "description": "A backpacking trip all over Europe",
                "id": "644938c180c5629beb5d04a8",
                "stops": [{
                    "name": "Venice",
                    "street": "1",
                    "state": "2",
                    "city": "Venice",
                    "description": "italy",
                    "id": "64493a5680c5629beb5d04ab",
                    "picture_url": "fakepic"
                    },
                    {
                    "name": "Italy",
                    "street": "1",
                    "state": "2",
                    "city": "Trieste",
                    "description": "A backpacking trip all over Europe",
                    "id": "64493ab280c5629beb5d04ac",
                    "picture_url": "fakepic"
                    }
                ]}
            ]}


def test_get_all_trips():
    # Arrange
    app.dependency_overrides[TripQueries] = FakeTripQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    # Act
    res = client.get('/api/trips')

    # Assert
    assert res.status_code == 200


def test_get_trip():
    # Arrange
    app.dependency_overrides[TripQueries] = FakeTripQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    # Act
    res = client.get('/api/trips/{trip_id}')

    # Assert
    assert res.status_code == 200


def test_get_stop():
    # Arrange
    app.dependency_overrides[TripQueries] = FakeTripQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    # Act
    res = client.get('/api/trips/{trip_id}/stops/{stop_id}')

    # Assert
    assert res.status_code == 200
