## March 20th, 2023:
On the current day, our team had acquired a GitLab repository and commenced collaboration towards designing our application. Through active discussion and analysis, we were able to refine our initial concepts and converge towards a single Minimum Viable Product (MVP), while also outlining our desired future goals. During this process, we completed our team's code of conduct, while also establishing a clear vision for the appearance and functionality of a simple profile page utilizing the standard Create, Read, Update, Delete (CRUD) operations.

## March 21st, 2023:
Our daily routine commenced with a standup meeting, wherein each team member articulated their previous day's accomplishments and delineated their aspirations for the collective progression. Subsequently, we delved into the intricate process of wireframing, meticulously crafting the visual blueprint of our frontend interface. To further augment our development endeavors, we deliberated over the selection of a fitting data platform, such as SQL or MongoDB, with plans to reconvene the following day to finalize our well-informed decision.

## March 22nd, 2023:

Today, we engaged in a comprehensive deliberation regarding the selection of an appropriate database for our project, choosing between MongoDB and SQL. After thoroughly examining the salient features of both, we ultimately opted for MongoDB, given its remarkable scalability and abstract nature. Subsequently, we devoted our attention to finalizing the wireframing process and strategizing an effective approach to assimilate the requisite literature for the forthcoming day's endeavors.

## March 23rd, 2023:

Today, we reached a consensus on our modus operandi for situations wherein a team member undertakes work beyond the predetermined scope outside our regular meetings. To address such instances, we agreed to convene the following day for a stand-up meeting, during which each individual would elucidate their respective contributions, highlighting any instances of working ahead. In the event that a team member had indeed advanced the project, the entire group would meticulously examine the accomplished tasks, ensuring a collective comprehension of the progress and facilitating seamless collaboration as we proceed.

## March 24rd, 2023:

Today, we engaged in our customary daily stand-up meeting, during which we diligently discussed our progress and addressed any pressing concerns. Following this intellectual exchange, we devoted our efforts towards the refinement and enhancement of the API design, ensuring that it aligned with the highest standards of sophistication and usability. Subsequently, we embarked on the creative endeavor of enriching the wireframe with the addition of an elegant homepage, ultimately culminating in a comprehensive and polished wireframe. Lastly, we concluded the day by meticulously finalizing the design of the API endpoints, thereby demonstrating our unwavering commitment to excellence in every aspect of our work.

## March 30th, 2023:

In today's standup session, we distributed backend tasks among team members. However, we ultimately agreed that assigning one person to spearhead the development process would be more efficient. All team members actively participated in coding and advancing the project. Our web page will feature a concise overview and elaborate mockups of the application's functionalities. Information about team members might be displayed in the footer section.

## April 4rd, 2023:

We achieved a milestone today by wrapping up the backend development and testing it using the FastAPI Swagger browser, with all components operating as expected. The team chose to spend the rest of the week revisiting explorations before tackling frontend development after the spring break.

## April 6th, 2023:

I started reviewing the necessary explorations and lectures to better understand the material and keep the momentum going. The much-awaited spring break is just around the corner, so we took it a bit easy.

## April 11th-14th, 2023:

We're on Spring Break!

## April 19th, 2023:

We initiated frontend development today. Following our API design, we devised a range of React components and interconnected them. The team worked together to incorporate and refine specific functionalities. This evening's goals involve delving deeper into FastAPI and conducting research on MongoDB/PostgreSQL to aid us in selecting the most suitable database system for our application.

# April 20th, 2023:

Our efforts have been concentrated on thoroughly examining our codebase, rectifying bugs, and ensuring a seamless experience without errors. Regrettably, we discovered that Ray has withdrawn from the program, a saddening turn of events. However, the team continues to march forward, undeterred.

## April 26th, 2023:

We successfully accomplished frontend development and confirmed the proper functioning of all elements. Our current priority is to integrate the design components to elevate the user interface and overall visual appeal.

## April 28th, 2023:

We carried out a comprehensive assessment of our application to guarantee its impeccable performance. We identified an issue with our logout feature: while it appeared to log out and remove the token, the home page remained accessible via the localhost:3000/trips URL even though the user was logged out. With guidance from our instructor, Riley, we pinpointed the root of the problem — an absent line of code within the logout function in the auth.js file. After rectifying the issue, the application operated seamlessly, and we are now poised for submission.
